import 'package:flutter/material.dart';
import 'package:maldives_reef_life/config/colors.dart';
// uncomment if you need to user stateless widget
// class Chip2 extends StatelessWidget {
//   final String name;
//   final bool isSelected;
//   Chip2({Key? key, required this.name, this.isSelected=false}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: (){},
//       child: Container(
//         padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//         margin: EdgeInsets.only(left: 10),
//         decoration: BoxDecoration(
//           gradient: isSelected ? mainGradient : null,
//           borderRadius: BorderRadius.circular(30),
//           border: Border.all(width: 1, style: BorderStyle.solid, color: isSelected? Colors.black : Colors.white)
//         ),
//         child: Center(
//           child: Text(name, style: TextStyle(color: Colors.white, fontSize: 15)),
//         )
//       ),
//     );
//   }
// }
class Chip2 extends StatefulWidget {
  final String name;
  final bool? isSelected;
  Chip2({Key? key, required this.name, this.isSelected}) : super(key: key);


  @override
  State<Chip2> createState() => _Chip2State();
}

class _Chip2State extends State<Chip2> {
  bool isSelected = false;
  @override
  void initState() {
    if(widget.isSelected!= null){
      isSelected = widget.isSelected!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          margin: EdgeInsets.only(left: 10),
          decoration: BoxDecoration(
              gradient: isSelected ? mainGradient : null,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(width: 1, style: BorderStyle.solid, color: isSelected? Colors.black : Colors.white)
          ),
          child:InkWell(
              highlightColor: Colors.transparent,
              splashFactory: NoSplash.splashFactory,
              borderRadius: BorderRadius.circular(30),
              onTap: (){
                setState(() {
                  isSelected =!isSelected;
                });
              },
              child: Center(
            child: Text(widget.name, style: TextStyle(color: Colors.white, fontSize: 15)),
          )
      ),
    );
  }
}