import 'package:flutter/material.dart';
import 'package:maldives_reef_life/config/theme.dart';
import 'package:maldives_reef_life/ui/screens/splash.dart';
import 'package:maldives_reef_life/ui/screens/welcome.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: themeData,
      home:  const SafeArea(child: SplashScreen()),
    );
  }
}
