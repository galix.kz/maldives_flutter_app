import 'package:flutter/material.dart';

Color backgroundColor = Color(0xFF181818);
Color grayBlackColor = Color(0xFF252525);
Color grayBlackColorActive = Color(0xFF6F6F6F);
Color mainPinkColor =    Color.fromRGBO(250, 102, 236, 1.0);

Gradient mainGradient = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [

    Color(0XFFFE5CEE),
    Color(0XFF884682),
  ],
);
Gradient mainGradientText = LinearGradient(
  begin: Alignment.topRight,
  end: Alignment.bottomLeft,
  colors: [
    Color(0XFFFE5CEE),
    Color(0XFF884682),
  ],
);

Gradient splashGradient = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [
    Color.fromRGBO(37, 37, 37, 1),
    Color.fromRGBO(250, 102, 236, 1.0),

  ],
);