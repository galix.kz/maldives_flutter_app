import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:maldives_reef_life/config/colors.dart';
import 'package:maldives_reef_life/ui/screens/home.dart';
import 'package:maldives_reef_life/ui/widgets/DropDownSelectField.dart';
import 'package:maldives_reef_life/ui/widgets/TextDescription.dart';
import 'package:maldives_reef_life/ui/widgets/TextTitle.dart';
import 'package:maldives_reef_life/ui/widgets/custom_button.dart';
import 'package:maldives_reef_life/ui/widgets/nav_bar_handler.dart';
import 'package:page_transition/page_transition.dart';
import 'package:url_launcher/url_launcher.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  List<String> list = <String>['One', 'Two', 'Three', 'Four'];
  String dropdownValue = 'One';
  bool scrolled = false;
  final ScrollController _controller = ScrollController();
  bool buttonDisabled = true;

// This is what you're looking for!
  void _scrollDown() {
    if (!scrolled) {
      _controller.animateTo(
        _controller.position.maxScrollExtent,
        duration: Duration(microseconds: 1200),
        curve: Curves.fastOutSlowIn,
      );
    } else {
      _controller.animateTo(
        0,
        duration: Duration(microseconds: 1200),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isSmallPhone = MediaQuery.of(context).size.width < 400;

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 20),
        child: ListView(
          controller: _controller,
          physics: (!scrolled) ? NeverScrollableScrollPhysics() : null,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: TextTitle(
                  title: 'FEEL BREEZE',
                  textAlign: TextAlign.start,
                  style: TextStyle(fontSize: (isSmallPhone) ? 26 : 30)),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: TextTitle(
                  title: 'MALDIVIAN BREEZE',
                  textAlign: TextAlign.start,
                  style: TextStyle(fontSize: (isSmallPhone) ? 26 : 30)),
            ),
            const SizedBox(height: 20),
            Image.asset('assets/img/image1.png'),
            SizedBox(height: (isSmallPhone) ? 20 : 40),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: RichText(
                    text: TextSpan(
                  children: [
                    TextSpan(
                        text:
                            'On the other hand, the strengthening and development ',
                        style: TextStyle(
                            fontSize: (isSmallPhone) ? 16 : 18,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w100)),
                    TextSpan(
                      text: 'of the structure',
                      style: TextStyle(
                          color: mainPinkColor,
                          decoration: TextDecoration.underline,
                          fontFamily: 'Poppins',
                          fontSize: (isSmallPhone) ? 16 : 18,
                          fontWeight: FontWeight.w100),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          launchUrl(Uri.parse(
                              'https://docs.flutter.io/flutter/services/UrlLauncher-class.html'));
                        },
                    ),
                    TextSpan(
                        text:
                            ' entails the process of the strengthening and development of the structure entails the process',
                        style: TextStyle(
                            fontSize: (isSmallPhone) ? 16 : 18,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w100))
                  ],
                ))),
            SizedBox(height: (isSmallPhone) ? 20: 70),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: CustomButton(
                title: 'START',
                onPressed: () {
                  Navigator.of(context).push(PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: NavBarHandler()));
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            // DropDownSelectField(placeholder: 'Select Region', options: list, onPressed: (){
            //   setState(() {
            //     buttonDisabled = false;
            //   });
            // },),
            // SizedBox(height: 10,),
            // // DropDownSelectField(placeholder: 'Select Region', options: list),
            // // SizedBox(height: 10,),
            // DropDownSelectField(placeholder: 'Select Region', options: list, onPressed: (){
            //   _scrollDown();
            //   setState(() {
            //     scrolled = !scrolled;
            //   });
            // }),
            // SizedBox(height: 100,),
          ],
        ),
      ),
    );
  }
}
