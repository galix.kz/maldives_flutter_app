import 'package:accordion/accordion.dart';
import 'package:accordion/controllers.dart';
import 'package:flutter/material.dart';
import 'package:maldives_reef_life/config/colors.dart';
import 'package:maldives_reef_life/ui/widgets/custom_dropdown.dart';

class FilterAccordion extends StatefulWidget {
  final Function onClose;

  const FilterAccordion({Key? key, required this.onClose}) : super(key: key);

  @override
  State<FilterAccordion> createState() => _FilterAccordionState();
}

class _FilterAccordionState extends State<FilterAccordion> {
  final _headerStyle = const TextStyle(
      color: Color(0xffffffff), fontSize: 15, fontWeight: FontWeight.bold);
  final _contentStyleHeader = const TextStyle(
      color: Color(0xff999999), fontSize: 14, fontWeight: FontWeight.w700);
  final _contentStyle = const TextStyle(
      color: Color(0xff999999), fontSize: 14, fontWeight: FontWeight.normal);
  final _loremIpsum =
      '''Lorem ipsum is typically a corrupted version of 'De finibus bonorum et malorum', a 1st century BC text by the Roman statesman and philosopher Cicero, with words altered, added, and removed to make it nonsensical and improper Latin.''';

  bool showFilter = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(color: grayBlackColor),
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('FILTER'),
              InkWell(
                onTap: () {
                  widget.onClose();
                },
                child: Icon(Icons.close, color: Colors.white),
              )
            ],
          ),
        ),
        CustDropDown(
          items: const [
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Week",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ],
          hintText: 'A',
          borderRadius: 0,
          onChanged: (val) {
            print(val);
          },
        ),
        CustDropDown(
          items: const [
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Week",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ],
          hintText: 'A',
          borderRadius: 0,
          onChanged: (val) {
            print(val);
          },
        ),
        CustDropDown(
          items: const [
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Week",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
            CustDropdownMenuItem(
              value: 0,
              child: Text(
                "Day",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ],
          hintText: 'A',
          borderRadius: 0,
          onChanged: (val) {
            print(val);
          },
        )
      ],
    );
  }
}
