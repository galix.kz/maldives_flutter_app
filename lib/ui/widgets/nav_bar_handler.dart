import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maldives_reef_life/config/colors.dart';
import 'package:maldives_reef_life/ui/screens/favourites.dart';
import 'package:maldives_reef_life/ui/screens/home.dart';
import 'package:maldives_reef_life/ui/screens/search.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class MenuItem {
  const MenuItem(this.icon, this.activeIcon, this.text);

  final Widget icon;
  final Widget activeIcon;
  final String text;
}

class NavBarHandler extends StatefulWidget {
  const NavBarHandler({Key? key}) : super(key: key);

  @override
  State<NavBarHandler> createState() => _DefaultScaffoldState();
}

class _DefaultScaffoldState extends State<NavBarHandler> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  // TODO: implement build
  final menuItemlist = [
    PersistentBottomNavBarItem(
        inactiveIcon: SvgPicture.asset('assets/img/catalog_inactive.svg'),
        icon: SvgPicture.asset('assets/img/catalog_active.svg')),
    PersistentBottomNavBarItem(
      inactiveIcon: Badge(
        alignment: const AlignmentDirectional(24, -4),
        label: Text('12'),
        backgroundColor: Color.fromRGBO(181, 75, 168, 1.0),
        child: SvgPicture.asset('assets/img/favourite_inactive.svg'),
      ),
      icon: Badge(
        label: Text('12'),
        alignment: const AlignmentDirectional(24, -4),
        backgroundColor: Color.fromRGBO(181, 75, 168, 1.0),
        child: SvgPicture.asset('assets/img/favourite_active.svg'),
      ),),
    PersistentBottomNavBarItem(
      inactiveIcon: SvgPicture.asset('assets/img/search_inactive.svg'),
      icon: SvgPicture.asset('assets/img/search_active.svg'),),
  ];
  int index = 0;
  PersistentTabController controller = PersistentTabController(initialIndex: 0);


  List<Widget> _buildScreens(PersistentTabController controller) {
    return <Widget>[
      HomeScreen(),
      FavouriteScreen(),
      SearchScreen(controller: controller)
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: controller,
      screens: _buildScreens(controller),
      items: menuItemlist,
      backgroundColor: Color(0XFF181818),
      // Default is Colors.white.
      handleAndroidBackButtonPress: true,
      // Default is true.
      resizeToAvoidBottomInset: true,
      // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true,
      // Default is true.
      hideNavigationBarWhenKeyboardShows: true,

      // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        colorBehindNavBar: Color(0XFF303030),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.white.withOpacity(0.12),
              blurRadius: 0.5,
            ),
          ]
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      screenTransitionAnimation: ScreenTransitionAnimation( // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      itemAnimationProperties: ItemAnimationProperties( // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      navBarStyle: NavBarStyle
          .simple, // Choose the nav bar style with this property.
    );
  }
}
