import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maldives_reef_life/config/colors.dart';
import 'package:maldives_reef_life/ui/widgets/TextDescription.dart';
import 'package:maldives_reef_life/ui/widgets/back_button.dart';
import 'package:maldives_reef_life/ui/screens/full_image_slider.dart';
import 'package:maldives_reef_life/ui/widgets/text_gradient.dart';
import 'package:page_transition/page_transition.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

import 'home.dart';

class DetailModel {
  String latinName;
  String commonName;
  String region;
  String food;
  String? freauency;
  String? description;
  String? imageUrl;

  DetailModel(
      {required this.latinName,
      required this.commonName,
      required this.region,
      required this.food,
      this.description,
      this.freauency,
      this.imageUrl});
}

class DetailsScreen extends StatefulWidget {
  const DetailsScreen({Key? key}) : super(key: key);

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  bool isActive = false;
  DetailModel detail = DetailModel(
      latinName: 'Nembrotha chamberlaini',
      commonName: 'Chamberlan’s Nembrotha',
      region: 'Indo-Pacific',
      food: 'Ascidians',
      freauency: 'Commo',
      description:
          'Morbi sit amet risus ornare, venenatis est con dimentum, elementum urna In dictum, elementum urna In dictum,amet risus ornare');

  final List<String> images = [
    'assets/img/detail_1.png',
    'assets/img/detail_1.png',
    'assets/img/detail_1.png',
    'assets/img/detail_1.png',
    'assets/img/detail_1.png',
    'assets/img/detail_1.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        color: backgroundColor,
        child: SafeArea(
            child: Scaffold(
                body: ListView(
                    padding:
                        const EdgeInsets.only(left: 15, right: 15, top: 30),
                    children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const BackButton2(),
                  const TextGradient(
                    'BEAR FAMILY',
                    style: TextStyle(fontSize: 20,),
                  )
                ],
              ),
              const SizedBox(height: 20),
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    height: 200,
                    child: CarouselSlider.builder(
                        unlimitedMode: true,
                        slideBuilder: (index) {
                          return Stack(
                            children: [
                              InkWell(
                                  onTap: () {
                                    PersistentNavBarNavigator.pushNewScreenWithRouteSettings(
                                      context,
                                      settings: RouteSettings(name: 'full_image'),
                                      screen: FullImageSlider(
                                        current: index, images: images),
                                      withNavBar: false,
                                      pageTransitionAnimation: PageTransitionAnimation.cupertino,
                                    );
                                    // Navigator.of(context).push(PageTransition(
                                    //     type: PageTransitionType.fade,
                                    //     child: FullImageSlider(
                                    //         current: index, images: images)));
                                  },
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Image.asset(images[index],
                                          width: double.infinity,
                                          fit: BoxFit.cover),
                                    ),
                                  )),
                              Positioned(
                                  right: 10,
                                  top: 10,
                                  child:
                                      SvgPicture.asset('assets/img/arrow.svg'))
                            ],
                          );
                        },
                        slideTransform: DefaultTransform(),
                        slideIndicator: CircularSlideIndicator(
                          indicatorRadius: 3,
                          currentIndicatorColor:
                              Color.fromRGBO(181, 75, 168, 1.0),
                          indicatorBorderColor: Colors.white30,
                          indicatorBorderWidth: 1,
                          itemSpacing: 12,
                          indicatorBackgroundColor: Colors.white,
                          padding: EdgeInsets.only(bottom: 10),
                        ),
                        itemCount: images.length),
                  ),
                  Positioned(
                      bottom: -30,
                      right: 0,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            isActive = !isActive;
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          // Adjust the padding as needed
                          child: Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              color: (!isActive) ? Colors.white60 : null,
                              gradient: (isActive) ? mainGradient : null,
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: (!isActive)
                                ? SvgPicture.asset('assets/img/fave_save.svg')
                                : SvgPicture.asset(
                                    'assets/img/favourite_inactive.svg'),
                          ),
                        ),
                      )),
                ],
              ),
              const SizedBox(height: 30),
              const TextGradient('Latin name',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              const SizedBox(height: 3),
              TextDescription(
                  style: TextStyle(
                      color: Color(0XFFD2D2D2),
                      fontWeight: FontWeight.w100,fontStyle: FontStyle.italic),
                  title: detail.latinName),
              const SizedBox(height: 10),
              const TextGradient('Common name',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              const SizedBox(height: 3),
              TextDescription(
                  style: TextStyle(
                      color: Color(0XFFD2D2D2), fontWeight: FontWeight.w100),
                  title: detail.commonName),
              const SizedBox(height: 10),
              const TextGradient('Region',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              const SizedBox(height: 3),
              TextDescription(
                  style: TextStyle(
                      color: Color(0XFFD2D2D2), fontWeight: FontWeight.w100),
                  title: detail.region),
              const SizedBox(height: 10),
              const TextGradient('Food',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              const SizedBox(height: 3),
              TextDescription(
                  style: TextStyle(
                      color: Color(0XFFD2D2D2), fontWeight: FontWeight.w100),
                  title: detail.food),
              const SizedBox(height: 10),
              const TextGradient('Freauency',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              const SizedBox(height: 3),
              TextDescription(
                  style: TextStyle(
                      color: Color(0XFFD2D2D2), fontWeight: FontWeight.w100),
                  title: detail.freauency ?? 'NO INFORMATION'),
              const SizedBox(height: 10),
              const TextGradient('Description',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              const SizedBox(height: 3),
              TextDescription(
                  style: TextStyle(
                      color: Color(0XFFD2D2D2), fontWeight: FontWeight.w100),
                  title: detail.description ?? ''),
            ]))));
  }
}
