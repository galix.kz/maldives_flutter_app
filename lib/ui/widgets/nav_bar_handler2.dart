import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maldives_reef_life/ui/screens/favourites.dart';
import 'package:maldives_reef_life/ui/screens/home.dart';
import 'package:maldives_reef_life/ui/screens/search.dart';

class MenuItem {
  const MenuItem(this.icon, this.activeIcon, this.text);

  final Widget icon;
  final Widget activeIcon;
  final String text;
}

class NavBarHandler extends StatefulWidget {
  const NavBarHandler({Key? key}) : super(key: key);

  @override
  State<NavBarHandler> createState() => _DefaultScaffoldState();
}

class _DefaultScaffoldState extends State<NavBarHandler> {
  // TODO: implement build
  final menuItemlist = <MenuItem>[
    MenuItem(SvgPicture.asset('assets/img/catalog_inactive.svg'),
        SvgPicture.asset('assets/img/catalog_active.svg'), 'Home'),
    MenuItem(
        Badge(
          label: Text('12'),
          backgroundColor: Color.fromRGBO(181, 75, 168, 1.0),
          child: SvgPicture.asset('assets/img/favourite_inactive.svg'),
        ),
        Badge(
          label: Text('12'),
          backgroundColor: Color.fromRGBO(181, 75, 168, 1.0),
          child: SvgPicture.asset('assets/img/favourite_active.svg'),
        ),
        'Products'),
    MenuItem(SvgPicture.asset('assets/img/search_inactive.svg'),
        SvgPicture.asset('assets/img/search_active.svg'), 'Me'),
  ];
  int index = 0;
  final _buildBody = <Widget>[HomeScreen(), FavouriteScreen(), SearchScreen()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Theme(
            data: ThemeData(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
            ),
            child: BottomNavigationBar(
              currentIndex: index,
              onTap: (x) {
                setState(() {
                  index = x;
                });
              },
              elevation: 16.0,
              backgroundColor: Colors.black26,
              showUnselectedLabels: true,
              unselectedItemColor: Colors.black26,
              selectedItemColor: Colors.black26,
              items: menuItemlist
                  .map((MenuItem menuItem) => BottomNavigationBarItem(
                        backgroundColor: Colors.black26,
                        icon: Container(
                            padding: EdgeInsets.only(top: 10),
                            child: menuItem.icon),
                        activeIcon:Container(
                            padding: EdgeInsets.only(top: 10),
                            child: menuItem.activeIcon),
                        label: '',
                      ))
                  .toList(),
            )),
        body: _buildBody[index]);
  }
}
