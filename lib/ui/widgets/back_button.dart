import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BackButton2 extends StatelessWidget {
  const BackButton2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: SvgPicture.asset('assets/img/back_icon.svg', width: 20,),
      )
    );
  }
}
