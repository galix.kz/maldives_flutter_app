import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:maldives_reef_life/ui/widgets/TextDescription.dart';
import 'package:maldives_reef_life/ui/widgets/text_gradient.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

import '../../config/colors.dart';

class SearchScreen extends StatefulWidget {
  PersistentTabController? controller;
  SearchScreen({Key? key, this.controller}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  bool noResults = false;
  static const List<String> _kOptions = <String>[
    'aardvark',
    'bobcat',
    'chameleon',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        color: backgroundColor,
        child: SafeArea(
            child: Scaffold(
                body: Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 20),
          decoration: BoxDecoration(
              color: grayBlackColor, borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          child: ListView(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
            children: [
              Stack(
                children: [Autocomplete<String>(fieldViewBuilder: (BuildContext context,
                    TextEditingController textEditingController,
                    FocusNode focusNode,
                    VoidCallback onFieldSubmitted) {
                  return TextFormField(
                    controller: textEditingController,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      hintText: 'Find animals on the zoo',
                      hintStyle: TextStyle(color: Color(0xff9E9E9E), fontSize: 14),
                      isDense: true,
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 40, vertical: 10),
                      prefixIconConstraints: const BoxConstraints(
                          maxHeight: 50,
                          maxWidth: 50,
                          minHeight: 20,
                          minWidth: 20),
                      fillColor: Colors.white,
                      prefixIcon: Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: SvgPicture.asset(
                            'assets/img/search_icon.svg',
                          )),
                      enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 1.0),
                      ),
                    ),
                    focusNode: focusNode,
                    onFieldSubmitted: (String value) {
                      onFieldSubmitted();
                      print('You just typed a new entry  $value');
                    },
                  );
                }, optionsBuilder: (TextEditingValue textEditingValue) {
                  if (textEditingValue.text == '') {
                    return const Iterable<String>.empty();
                  }
                  var found = _kOptions.where((String option) {
                    return option.contains(textEditingValue.text.toLowerCase());
                  });
                  if (found.isEmpty) {
                    setState(() {
                      noResults = true;
                    });
                  }
                  return found;
                }, optionsViewBuilder: (context, onAutoCompleteSelect, options) {
                  return Align(
                      alignment: Alignment.topLeft,
                      child: Material(
                        color: grayBlackColor,
                        elevation: 4.0,
                        // size works, when placed here below the Material widget
                        child: Container(
                          // I have the text field wrapped in a container with
                          // EdgeInsets.all(20) so subtract 40 from the width for the width
                          // of the text box. You could also just use a padding widget
                          // with EdgeInsets.only(right: 20)
                            width: MediaQuery.of(context).size.width - 40,
                            child: ListView.separated(
                              shrinkWrap: true,
                              padding: const EdgeInsets.all(8.0),
                              itemCount: options.length,
                              separatorBuilder: (context, i) {
                                return const Divider();
                              },
                              itemBuilder: (BuildContext context, int index) {
                                final String option = options.elementAt(index);
                                // some child here
                                return Padding(
                                    padding:
                                    const EdgeInsets.symmetric(vertical: 20),
                                    child: Text('${option}'));
                              },
                            )),
                      ));
                }, onSelected: (String selection) {
                  debugPrint('You just selected $selection');
                }),
                  Positioned(
                    right: 0,
                      top: 0,
                      child:  InkWell(
                    onTap: () {
                      setState(() {
                        noResults = false;
                      });
                      if(widget.controller!= null) {
                        widget.controller!.jumpToTab(0);
                      }
                    },
                    child: const Icon(Icons.close,
                        color: Color(0XFF5F5F5F), size: 25),
                  ))

                ],
              ),

              if (noResults)
                SizedBox(
                    height: 600,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        TextGradient('No results',
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center),
                        SizedBox(height: 20),
                        TextDescription(
                            title:
                                'Try to enter another query, there is no such animal',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w100),
                            textAlign: TextAlign.center)
                      ],
                    ))
            ],
          ),
        ))));
  }
}
