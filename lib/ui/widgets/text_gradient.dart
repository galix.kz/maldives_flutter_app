import 'package:flutter/material.dart';
import 'package:maldives_reef_life/config/colors.dart';

class TextGradient extends StatelessWidget {
  const TextGradient(
      this.text, {
        this.style,
        this.textAlign,
      });

  final String text;
  final TextStyle? style;
  final TextAlign? textAlign ;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcIn,
      shaderCallback: (bounds) => mainGradientText.createShader(
        Rect.fromLTWH(0, 0, bounds.width, bounds.height),
      ),
      child: Text(text, style: TextStyle(fontSize: 17).merge(style), textAlign: textAlign),
    );
  }
}
