import 'package:flutter/material.dart';

class TextTitle extends StatelessWidget {
  final String title;
  final TextStyle? style;
  final TextAlign? textAlign;

  const TextTitle({Key? key, required this.title, this.style, this.textAlign})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(title,
        style: Theme.of(context).textTheme.headlineMedium!.merge(style), textAlign: textAlign,);
  }
}
