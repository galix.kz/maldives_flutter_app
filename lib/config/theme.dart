import 'package:flutter/material.dart';

ThemeData themeData = ThemeData(
  // Define the default brightness and colors.
  scaffoldBackgroundColor: Color(0xFF181818),
  primaryColor: Colors.white,

  primarySwatch: Colors.deepOrange,
  brightness: Brightness.light,
  // Define the default font family.
  fontFamily: 'Poppins',
  textTheme: const TextTheme(
    headlineMedium: TextStyle(
      fontFamily: 'Poppins',
        fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
    bodyMedium: TextStyle(
        fontFamily: 'Poppins',
        fontSize: 18, color: Colors.white),
  ),
// Define the default `TextTheme`. Use this to specify the default
// text styling for headlines, titles, bodies of text, and more.
);
