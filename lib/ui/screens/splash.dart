import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gradient_widgets/gradient_widgets.dart';
import 'package:maldives_reef_life/config/colors.dart';
import 'package:maldives_reef_life/ui/screens/welcome.dart';
import 'package:page_transition/page_transition.dart';

import 'home.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 2), () {
      Navigator.pushReplacement(
        context,
        PageTransition(type: PageTransitionType.fade, child: WelcomeScreen()),
      );
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset('assets/img/sss.svg'),
          SizedBox(height: 100),
          GradientCircularProgressIndicator(
            gradient: splashGradient,

            radius: 90,
          ),
        ],
      ),
    ));
  }
}
