import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maldives_reef_life/config/colors.dart';
import 'package:maldives_reef_life/ui/screens/category.dart';
import 'package:maldives_reef_life/ui/screens/details.dart';
import 'package:maldives_reef_life/ui/widgets/animal_card.dart';
import 'package:maldives_reef_life/ui/widgets/chip.dart';
import 'package:maldives_reef_life/ui/widgets/filter_accordion.dart';
import 'package:page_transition/page_transition.dart';

class FavouriteScreen extends StatefulWidget {
  const FavouriteScreen({Key? key}) : super(key: key);

  @override
  State<FavouriteScreen> createState() => _FavouriteScreenState();
}

class _FavouriteScreenState extends State<FavouriteScreen> {
  var categories = [
    'All animals',
    'From europe',
    'From Asia',
    'From Australia'
  ];
  var animals = [
    {
      'name': 'BEAR FAMILY',
      'image': 'assets/img/1.png',
    },
    {
      'name': 'BEAR FAMILY',
      'image': 'assets/img/4.png',
    },
    {
      'name': 'BEAR FAMILY',
      'image': 'assets/img/3.png',
    },
    {
      'name': 'BEAR FAMILY',
      'image': 'assets/img/2.png',
    },
    {
      'name': 'BEAR FAMILY',
    }
  ];
  bool showFilter = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        color: backgroundColor,
        child: SafeArea(
            child: Scaffold(
                body: Container(
                    padding: EdgeInsets.only(top: 20, left: 10, right: 10),
                    child: Stack(children: [
                      ListView(
                        padding: EdgeInsets.zero,
                        children: [
                          // Container(
                          //     height: 50,
                          //     child: Row(
                          //       children: [
                          //         if (!showFilter)
                          //           InkWell(
                          //             onTap: () {
                          //               setState(() {
                          //                 showFilter = true;
                          //               });
                          //             },
                          //             child: SvgPicture.asset(
                          //                 'assets/img/filter_icon.svg'),
                          //           ),
                          //       ],
                          //     )),
                          // const SizedBox(height: 20),
                          GridView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 10.0,
                              mainAxisSpacing: 20.0,
                              childAspectRatio: 1.2,
                            ),
                            itemCount: animals.length,
                            itemBuilder: (context, index) {
                              return AnimalCard(
                                name: animals[index]['name']!,
                                imageUrl: animals[index]['image'],
                                onTap: () {

                                  Navigator.of(context).push(PageTransition(
                                      type: PageTransitionType.rightToLeft,child: DetailsScreen()));
                                },
                                onDelete: () {},
                              );
                            },
                          ),
                        ],
                      ),
                      if (showFilter)
                        FilterAccordion(
                          onClose: () {
                            setState(() {
                              showFilter = false;
                            });
                          },
                        )
                    ])))));
  }
}
