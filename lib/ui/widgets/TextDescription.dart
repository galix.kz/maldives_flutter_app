import 'package:flutter/material.dart';

class TextDescription extends StatelessWidget {
  final String title;
  final TextStyle? style;
  final TextAlign? textAlign;

  const TextDescription({Key? key, required this.title, this.style, this.textAlign})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(title,
      style: Theme.of(context).textTheme.bodyMedium!.merge(style), textAlign: textAlign,);
  }
}
