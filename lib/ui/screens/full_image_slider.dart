import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:maldives_reef_life/ui/widgets/back_button.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class FullImageSlider extends StatefulWidget {
  final int current;
  final List<String> images;

  const FullImageSlider({Key? key, required this.current, required this.images})
      : super(key: key);

  @override
  _FullImageSliderState createState() => _FullImageSliderState();
}

class _FullImageSliderState extends State<FullImageSlider> {
  int _current = 0;
  bool _stateChange = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: Scaffold(
            body: Container(
                padding: EdgeInsets.only(top: 20),
                color: Colors.black,
                child: Stack(
                  children: [
                    PhotoViewGallery.builder(
                      scrollPhysics: const BouncingScrollPhysics(),

                      builder: (BuildContext context, int index) {
                        return PhotoViewGalleryPageOptions(
                          imageProvider: AssetImage(widget.images[index]),
                          initialScale: PhotoViewComputedScale.contained * 0.8,
                          heroAttributes: PhotoViewHeroAttributes(tag: index),
                        );
                      },
                      itemCount: widget.images.length,
                      loadingBuilder: (context, event) => Center(
                        child: Container(
                          width: 20.0,
                          height: 20.0,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                      // backgroundDecoration: widget.backgroundDecoration,
                      // pageController: widget.pageController,
                      // onPageChanged: onPageChanged,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding: EdgeInsets.only(right: 30, top: 0, bottom: 30),
                            child: SvgPicture.asset(
                              'assets/img/back_icon.svg',
                              width: 20,
                            ),
                          )),
                    )
                  ],
                ))));
  }
}
