import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AnimalCard extends StatelessWidget {
  final String name;
  final String? imageUrl;
  final Function onTap;
  final Function? onDelete;

  const AnimalCard(
      {Key? key,
      required this.name,
      this.imageUrl,
      required this.onTap,
      this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InkWell(
            onTap: () {
              onTap();
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: Container(
                  height: 70,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.grey,
                    image: (imageUrl != null)
                        ? DecorationImage(
                            image: AssetImage(imageUrl!),
                            fit: BoxFit.cover,
                          )
                        : DecorationImage(
                            image: AssetImage('assets/img/nophoto.png'),
                            fit: BoxFit.fitHeight,
                          ),
                  ),
                )),
                SizedBox(height: 10),
                Text(
                  name,
                  style: TextStyle(
                      color: Color(0XFFFFEFFC), fontWeight: FontWeight.bold, fontSize: 16),
                  textAlign: TextAlign.left,
                )
              ],
            )),
        if (onDelete != null)
          Align(
              alignment: Alignment.topRight,
              child: InkWell(
                  child: Container(
                      padding: EdgeInsets.all(5),
                      margin: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white60,
                          borderRadius: BorderRadius.circular(10)),
                      child: SvgPicture.asset(
                        'assets/img/delete.svg',
                      )),
                  onTap: () {
                    onDelete!();
                  })),
      ],
    );
  }
}
