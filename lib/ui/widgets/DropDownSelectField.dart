
import 'package:flutter/material.dart';
import 'package:maldives_reef_life/ui/widgets/TextDescription.dart';
import 'package:maldives_reef_life/ui/widgets/custom_dropdown.dart';

import '../../config/colors.dart';

class DropDownSelectField extends StatefulWidget {
  String placeholder;
  String? value;
  List<String> options;
  VoidCallback? onPressed;


  DropDownSelectField({Key? key, required this.placeholder, required this.options, this.value, this.onPressed})
      : super(key: key);

  @override
  State<DropDownSelectField> createState() => _DropDownSelectFieldState();
}

class _DropDownSelectFieldState extends State<DropDownSelectField> {
  String localValue = '';
  bool isExpanded = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child:  CustDropDown(
        onSelected: widget.onPressed,

      items: const [
        CustDropdownMenuItem(
          value: 0,
          child: Text("Day", style:TextStyle(color: Colors.white, fontSize: 16),),
        ),
        CustDropdownMenuItem(
          value: 0,
          child: Text("Week",style:TextStyle(color: Colors.white, fontSize: 16),),
        ),
        CustDropdownMenuItem(
          value: 0,
          child: Text("Day", style:TextStyle(color: Colors.white, fontSize: 16),),
        ),
        CustDropdownMenuItem(
          value: 0,
          child: Text("Day", style:TextStyle(color: Colors.white, fontSize: 16),),
        ),
        CustDropdownMenuItem(
          value: 0,
          child: Text("Day", style:TextStyle(color: Colors.white, fontSize: 16),),
        ),
        CustDropdownMenuItem(
          value: 0,
          child: Text("Day", style:TextStyle(color: Colors.white, fontSize: 16),),
        ),
        CustDropdownMenuItem(
          value: 0,
          child: Text("Day", style:TextStyle(color: Colors.white, fontSize: 16),),
        ),
      ],
      hintText: widget.placeholder,
      borderRadius: 5,
        onChanged: (val){
          print(val);
          if(widget.onPressed!=null) widget.onPressed!();
        },
    ));
  }
}
