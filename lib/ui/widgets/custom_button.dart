import 'package:flutter/material.dart';
import 'package:maldives_reef_life/config/colors.dart';

class CustomButton extends StatelessWidget {
  String title;
  Function onPressed;
  bool disabled=false;
   CustomButton({Key? key, required this.title, required this.onPressed, this.disabled=false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  InkWell(
        onTap: () {
          if(!disabled){
            onPressed();
          }
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: disabled? Colors.grey[300] : null,
            gradient: !disabled  ? mainGradient : null,
          ),
          child: Center(child: Text(title,style: TextStyle(color: (disabled) ? Colors.black : Colors.white, fontFamily: 'Poppins'))),
        )
    );
  }
}
